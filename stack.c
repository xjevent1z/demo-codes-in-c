/**
 * @file stack.c
 * @author Jung Te Chou (qq3025566@gmail.com)
 * @brief A program to generate Fibonacci number and store them to a stack
 * @version 1.0
 * @date 2021-11-05
 *
 * @copyright Copyright (c) 2021
 *
 */
#include "stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Stack *New(unsigned int size) {
    Stack *stack = (Stack *)malloc(sizeof(Stack));
    stack->size = size;
    stack->array = (unsigned int *)malloc(sizeof(unsigned int) * size);
    stack->top = -1;
    stack->Pop = Pop;
    stack->Push = Push;
    stack->Peek = Peek;
    stack->IsEmpty = IsEmpty;
    stack->IsFull = IsFull;
    return stack;
}

unsigned int Pop(Stack *stack) {
    if (stack->IsEmpty(stack)) {
        return INT_MIN;
    }
    return stack->array[stack->top--];
}

void Push(Stack *stack, int item) {
    if (stack->IsFull(stack)) {
        return;
    }
    stack->array[++stack->top] = item;
    return;
}

unsigned int Peek(Stack *stack) {
    if (stack->IsEmpty(stack))
        return INT_MIN;
    return stack->array[stack->top];
}

bool IsEmpty(Stack *stack) {
    return stack->top == -1;
}

bool IsFull(Stack *stack) {
    return stack->top == stack->size - 1;
}

/**
 * @brief Entry Point
 *
 * @param argc
 * @param argv
 * @return int
 */
int main(int argc, char **argv) {
    unsigned int size = 0;
    unsigned int *fib = NULL;

    for (char **arg = argv + 1; arg != argv + argc; arg++) {
        if (!strncmp(*arg, "-n", 2)) {
            size = (unsigned int)atoi(*(arg + 1)) + 1;
            if (size >= MAXSIZE) {
                size = MAXSIZE;
            }
        }
    }

    Stack *stack = New(size);
    fib = (unsigned int *)malloc(sizeof(unsigned int) * size);
    memset(fib, 0, sizeof(unsigned int) * size);
    for (unsigned int i = 0; i < size; i++) {
        if (i == 0 || i == 1) {
            fib[i] = 1;
        } else {
            fib[i] = fib[i - 1] + fib[i - 2];
        }
        stack->Push(stack, fib[i]);
        printf("%d = %d\n", i, stack->Peek(stack));
    }
    return 0;
}
